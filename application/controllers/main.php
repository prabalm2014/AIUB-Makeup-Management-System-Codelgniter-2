<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Main extends CI_Controller {
	
	public function index()
	{
		/*$this->load->model('makeup');
		$data['notice_list'] = $this->makeup->notice();
		$this->load->library('parser');
		$this->parser->parse('login', $data);*/
		$this->load->view('login');
	}
	public function verify()
	{
		$this->form_validation->set_rules('id', 'Id', 'required|callback_checkId');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run() == false)
		{
			/*$this->load->model('makeup');
			$data['notice_list'] = $this->makeup->notice();
			$this->load->library('parser');
			$this->parser->parse('login', $data);*/
			$this->load->view('login');
		}
		else
		{
			if($this->input->get_post('submit'))
			{
				$data['id'] = $this->input->post('id');
				$data['password'] = $this->input->post('password');
				
				$this->load->model('makeup');
				$result = $this->makeup->login($data);

				if($result == true)
				{
					$session = array(
						'id' => $this->input->post('id')
					);
					$this->session->set_userdata('open', $session);
					$this->load->helper('url');
					$admin = "/^[0-2][0-9][0-9][0-9]-[0-9]{3}-[1-3]$/";
					$regex = "/^[0-1][0-9]-[0-9]{5}-[1-3]$/";
					
					if(preg_match($admin, $data['id']))
					{
						redirect('http://localhost/php/main/admin', 'refresh');
					}
					else if(preg_match($regex, $data['id']))
					{
						redirect('http://localhost/php/main/faculty', 'refresh');
					}
				}
				else
				{
					echo "<p align=center style='border: 2px solid red;margin-left:40%;margin-right:40% ;padding:10px;margin-top:20%;border-radius:5px;font-size:22px;color:red;font-family: 'Times New Roman', Times, serif;color:#ffffb3;';>ID and Password not matched!<a href=http://localhost/php/main/verify> Try again</a></p>";
				}
			}
		}
	}
	
	public function sign()
	{
		$this->form_validation->set_rules('name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('type', 'Member Type', 'required');
		$this->form_validation->set_rules('id', 'Id', 'required|callback_checkId');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if($this->form_validation->run() == false)
		{
			$this->load->view('signup');
		}
		else
		{
			if($this->input->get_post('signupBtn'))
			{
				$data['name'] = $this->input->post('name');
				$data['email'] = $this->input->post('email');
				$data['type'] = $this->input->post('type');
				$data['id'] = $this->input->post('id');
				$data['password'] = $this->input->post('password');
				
				$this->load->model('makeup');
				$result = $this->makeup->signup($data);
	
				$this->load->helper('url');
				redirect('http://localhost/php/main/verify', 'refresh');

			}
		}
	}

	public function checkId($str)
	{
		$admin = "/^[0-2][0-9][0-9][0-9]-[0-9]{3}-[1-3]$/";
		$regex = "/^[0-1][0-9]-[0-9]{5}-[1-3]$/";
		if(preg_match($admin, $str))
		{
			return true;
		}
		else if(preg_match($regex, $str))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('checkId', 'ID usually looks like XX-XXXXX-X or XXXX-XXX-X.');
			return false;
		}
	}
	
	public function admin()
	{
		$this->load->model('makeup');
		$data['history_list'] = $this->makeup->showAllHistory2();
		$data['routine_list'] = $this->makeup->showSun();
		$data['routine_list1'] = $this->makeup->showMon();
		$data['routine_list2'] = $this->makeup->showTus();
		$data['routine_list3'] = $this->makeup->showWed();
		$data['routine_list4'] = $this->makeup->showThu();
		$data['routine_list5'] = $this->makeup->showFri();
		$data['routine_list6'] = $this->makeup->showSat();
		$this->load->library('parser');
		$this->parser->parse('admin', $data);
		$this->load->library('calendar');
		
		$calendar = array(
				'cal' => $this->calendar->generate()
			);
		$this->session->set_userdata('date', $calendar);
	}

	public function faculty()
	{
		$this->load->model('makeup');
		$data['history_list'] = $this->makeup->showAllHistory();
		$this->load->library('parser');
		$this->parser->parse('faculty', $data);
	}
	public function roomBook()
	{
		$this->load->view('bookRoom');
	}
	public function bookTime()
	{
		$data = $this->session->userdata('dayHour');
		$this->load->model('makeup');
		$routine_list = $this->makeup->viewRoutine();
		$data['routine_list'] = $routine_list;

		if($data['hour'] == "1.30")
		{
			$time_result = $this->makeup->oneHalfTime();
			$data['time_result'] = $time_result;
			$this->load->view('roomTime', $data);
		}
		else if($data['hour'] == "2")
		{
			$time_result = $this->makeup->twoHourTime();
			$data['time_result'] = $time_result;
			$this->load->view('roomTime', $data);
		}
		else
		{
			$time_result = $this->makeup->threeHourTime();
			$data['time_result'] = $time_result;
			$this->load->view('roomTime', $data);
		}
		$this->load->library('calendar');
		
		$calendar = array(
				'cal' => $this->calendar->generate()
			);
		$this->session->set_userdata('date', $calendar);
	}
	public function addDayHour()
	{
		$data['day'] = $this->input->post('day');
		$data['hour'] = $this->input->post('hour');
		if($this->input->get_post('submit'))
		{
			$dayhour = array(
				'day' => $this->input->post('day'),
				'hour' => $this->input->post('hour')
			);
			$this->session->set_userdata('dayHour', $dayhour);
			//$data = $this->session->userdata('dayHour');
			$this->load->helper('url');
			redirect('http://localhost/php/main/bookTime', 'refresh');
		}
	}

	public function request()
	{
		$data['sub'] = $this->input->post('sub');
		$data['room'] = $this->input->post('room');
		$data['time'] = $this->input->post('time');
		$data['date'] = $this->input->post('date');

		$timedatesub = array(
			'sub' => $this->input->post('sub'),
			'room' => $this->input->post('room'),
			'time' => $this->input->post('time'),
			'date' => $this->input->post('date')
		);
		$this->session->set_userdata('timedatesub', $timedatesub);

		if($data['time'] == "8.00 - 9.30")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "9.30 - 11.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "11.00 - 12.30")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "12.30 - 2.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "2.00 - 3.30")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "3.30 - 5.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "5.00 - 6.30")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "6.30 - 8.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "8.00 - 10.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "10.00 - 12.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "12.00 - 2.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "2.00 - 4.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "4.00 - 6.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "6.00 - 8.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "8.00 - 11.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "11.00 - 2.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "2.00 - 5.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
		else if($data['time'] == "5.00 - 8.00")
		{
			$this->load->model('makeup');
			$det = $this->makeup->time();
			$this->load->helper('url');
			redirect('http://localhost/php/main/faculty', 'refresh');
		}
	}
	public function profile($id)
	{
		$this->load->model('makeup');
		$data = $this->makeup->showProfile($id);
	
		$this->load->library('parser');
		$this->parser->parse('proView', $data);
	}
	public function adprofile($id)
	{
		$this->load->model('makeup');
		$data = $this->makeup->showProfile($id);
	
		$this->load->library('parser');
		$this->parser->parse('viewProAdmin', $data);
	}
	public function accept($id)
	{
		$this->load->model('makeup');
		$data = $this->makeup->acceptReq($id);
		$this->load->helper('url');
		redirect('http://localhost/php/main/admin', 'refresh');
	}
	public function reject($id)
	{
		$this->load->model('makeup');
		$data = $this->makeup->rejectReq($id);
		$this->load->helper('url');
		redirect('http://localhost/php/main/admin', 'refresh');
	}
	public function logout()
	{
		$this->session->sess_destroy();
		$this->load->helper('url');
		redirect('http://localhost/php/main/', 'refresh');
	}
	public function addNewRoom()
	{
		$this->load->view('addroutine');
		$data['day'] = $this->input->post('day');
		$data['room'] = $this->input->post('room');
		$data['t1'] = $this->input->post('t1');
		$data['t2'] = $this->input->post('t2');
		$data['t3'] = $this->input->post('t3');
		$data['t4'] = $this->input->post('t4');
		$data['t5'] = $this->input->post('t5');
		$data['t6'] = $this->input->post('t6');
		$data['t7'] = $this->input->post('t7');
		$data['t8'] = $this->input->post('t8');
		$data['t9'] = $this->input->post('t9');
		$data['t10'] = $this->input->post('t10');
		$data['t11'] = $this->input->post('t11');
		$data['t12'] = $this->input->post('t12');
		$data['t13'] = $this->input->post('t13');
		$data['t14'] = $this->input->post('t14');
		$data['t15'] = $this->input->post('t15');
		$data['t16'] = $this->input->post('t16');
		$data['t17'] = $this->input->post('t17');
		$data['t18'] = $this->input->post('t18');
		$data['t19'] = $this->input->post('t19');
		$data['t20'] = $this->input->post('t20');
		$data['t21'] = $this->input->post('t21');
		$data['t22'] = $this->input->post('t22');
		$data['t23'] = $this->input->post('t23');
		$data['t24'] = $this->input->post('t24');
		
		$day = array(
			'day' => $this->input->post('day')
		);
		$this->session->set_userdata('day', $day);

		if($this->input->get_post('submit'))
		{
			$this->load->model('makeup');
			$this->makeup->newRoom($data);
			$this->load->helper('url');
			redirect('http://localhost/php/main/admin', 'refresh');
		}
	}
	
}
