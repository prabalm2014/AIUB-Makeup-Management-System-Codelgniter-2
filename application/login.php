<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<style>
			body{
				background-color: #EBFFE0;
			}
			#opa{
				margin-top: 12%;
				position:fixed;
				margin-left: 60%;
				margin-right: 0%;
				background-color: #66143D;
				color: white;
				padding-right: 15px;
				padding-left: 15px;
			}
			.main{
				
				height:100%;
				width:100%;
				position:absolute;
				left: 0px;
			    top: 0px;
			}
			.tf{
				margin-top: 10px;
				height:30px;
				width:300px;
				font-size: 16px;
			}
			.bo{
				color: red;
			}
			h1{
				margin-bottom: -5px;
				padding-top:10px; 
			}
			.tfs{
				margin-top: 10px;
				height:38px;
				width:308px;
				font-size: 16px;
			}
			.timeline{
				height:10%;
				width:60%;
			    
			}
			fieldset{
				margin-left: 10%;
				margin-right: 20%;
				margin-top: 3%;
				border:2px solid #661F29;
				font-size: 18px;
				background-color: #FCFAFF;
				color: blue;
			}
			.h{
				color:#66241F;
				margin-left: 10%;
			}
		</style>
	</head>
	<body lang="en-US">
			<div class="main">
			<div id="opa" align="center">
				<h1>Log In</h1>
				<form action="/php/main/verify" method="post">
					<input class="tf" type="text" name="id" placeholder=" ID" value="<?php echo set_value('id'); ?>"></br>
					<input class="tf" type="password" name="password"  placeholder=" Password"></br>
					<input class="tfs" name="submit" type="submit" value="Log in" /><br/><br/>
					
					<div style="color: red;">
						<?php 
						echo 
						"<div style=padding:1px;>
						".validation_errors()."
						</div>"; 
						?>
					</div><br/>
				</form>
			</div>

			<div class="timeline">
			<h1 class="h">Notice</h1>
				{notice_list}
					<div align="center">
						<fieldset>
							<legend>Makeup Class</legend>
							<label>Subject : {subject}</label><br/>
							<label>Day : {day}</label><br/>
							<label>Room : {room}</label><br/>
							<label>Time : {class_time}</label><br/>
							<label>Faculty : {name}</label><br/>
						</fieldset>
					</div>
				{/notice_list}
				<br/><br/>
			</div>
			<br/>
			</div>
	</body>
</html>
