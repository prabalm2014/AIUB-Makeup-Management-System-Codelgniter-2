<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Makeup extends CI_Model {
	
	public function login($data) 
	{
		$sql = "SELECT User_Id,Password FROM user WHERE User_Id = '".$data['id']."' and Password = '".$data['password']."'";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->row_array(); 
	}
	public function signup($data) 
	{
		$this->load->database();
		$sql = "INSERT INTO user VALUES ('$data[id]', '$data[password]','$data[type]','$data[name]', '$data[email]' )";
		$this->db->query($sql);
	}
	public function viewRoutine()
	{
		$data = $this->session->userdata('dayHour');
		$sql = "SELECT * from ".$data['day']." ";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function viewRoom()
	{
		$data = $this->session->userdata('dayHour');
		$sql = "SELECT * from ".$data['day']." ";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function oneHalfTime()
	{
		$sql = 'SELECT * FROM times where t_id BETWEEN 1 and 8';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function twoHourTime()
	{
		$sql = 'SELECT * FROM times where t_id BETWEEN 9 and 14';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function threeHourTime()
	{
		$sql = 'SELECT * FROM times where t_id BETWEEN 15 and 18';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function time()
	{
		$dat = $this->session->userdata('dayHour');
		$data = $this->session->userdata('timedatesub');
		$de = $this->session->userdata('open');
		
		$this->load->database();
		if($data['time'] == "8.00 - 9.30")
		{
			$sql = "UPDATE ".$dat['day']." SET T1 = 'M', T2 = 'M', T3 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "9.30 - 11.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T4 = 'M', T5 = 'M', T6 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "11.00 - 12.30")
		{
			$sql = "UPDATE ".$dat['day']." SET T7 = 'M', T8 = 'M', T9 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "12.30 - 2.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T10 = 'M', T11 = 'M', T12 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "2.00 - 3.30")
		{
			$sql = "UPDATE ".$dat['day']." SET T13 = 'M', T14 = 'M', T15 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "3.30 - 5.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T16 = 'M', T17 = 'M', T18 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "5.00 - 6.30")
		{
			$sql = "UPDATE ".$dat['day']." SET T19 = 'M', T20 = 'M', T21 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "6.30 - 8.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T22 = 'M', T23 = 'M', T24 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "8.00 - 10.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T1 = 'M', T2 = 'M', T3 = 'M', T4 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "10.00 - 12.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T5 = 'M', T6 = 'M', T7 = 'M', T8 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "12.00 - 2.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T9 = 'M', T10 = 'M', T11 = 'M', T12 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "2.00 - 4.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T13 = 'M', T14 = 'M', T15 = 'M', T16 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "4.00 - 6.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T17 = 'M', T17 = 'M', T19 = 'M', T20 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "6.00 - 8.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T21 = 'M', T22 = 'M', T23 = 'M', T24 = 'M' where room = ".$data['room']." ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "8.00 - 11.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T1 = 'M', T2 = 'M', T3 = 'M', T4 = 'M', T5 = 'M', T6 = 'M' where room = '$data[room]' ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "11.00 - 2.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T7 = 'M', T8 = 'M', T9 = 'M', T10 = 'M', T11 = 'M', T12 = 'M' where room = '$data[room]' ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "2.00 - 5.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T13 = 'M', T14 = 'M', T15 = 'M', T16 = 'M', T17 = 'M', T18 = 'M' where room = '$data[room]' ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		else if($data['time'] == "5.00 - 8.00")
		{
			$sql = "UPDATE ".$dat['day']." SET T19 = 'M', T20 = 'M', T21 = 'M', T22 = 'M', T23 = 'M', T24 = 'M' where room = '$data[room]' ";
			$this->db->query($sql);

			$sql1 = "INSERT INTO makeuphistory VALUES (null,'$data[sub]', '$dat[day]','$data[room]','$data[time]', '$dat[hour]', '$de[id]' , '$data[date]', 'Requested' )";
			$this->db->query($sql1);
		}
		
	}

	public function showAllHistory()
	{
		$data = $this->session->userdata('open');
		$sql = "SELECT * FROM makeuphistory where User_Id = '$data[id]' ORDER BY h_id DESC";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	
	public function showAllHistory2()
	{
		$sql = 'SELECT * FROM makeuphistory ORDER BY h_id DESC';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}

	public function showProfile($id)
	{
		$this->load->database();
		$sql = 'SELECT * FROM user where User_Id = "'.$id.'"';
		$result = $this->db->query($sql);
		return $result->row_array();
	}

	public function showSun()
	{
		$sql = 'SELECT * FROM sunday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showMon()
	{
		$sql = 'SELECT * FROM monday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showTus()
	{
		$sql = 'SELECT * FROM tuesday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showWed()
	{
		$sql = 'SELECT * FROM wednesday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showThu()
	{
		$sql = 'SELECT * FROM thursday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showFri()
	{
		$sql = 'SELECT * FROM friday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function showSat()
	{
		$sql = 'SELECT * FROM saturday';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function acceptReq($id)
	{
		$this->load->database();
		$sql = "UPDATE makeuphistory SET status ='Accepted' where h_id = '$id' ";
		$this->db->query($sql);
	}
	public function rejectReq($id)
	{
		$this->load->database();
		$sql = "UPDATE makeuphistory SET status ='Rejected' where h_id = '$id' ";
		$this->db->query($sql);
	}
	/*public function notice()
	{
		$this->load->database();
		//$sql = "SELECT * from makeuphistory where status ='Accepted' ORDER BY h_id DESC";
		$sql = "SELECT * FROM makeuphistory join user on user.User_Id = makeuphistory.User_Id where status ='Accepted' ORDER BY h_id desc";
		$result = $this->db->query($sql);
		return $result->result_array();
	}*/
	public function newRoom($data)
	{
		$day = $this->session->userdata('day');
		$this->load->database();
		$sql = "INSERT INTO ".$day['day']." VALUES (null, '$data[room]','$data[t1]','$data[t2]', '$data[t3]', '$data[t4]', '$data[t5]', '$data[t6]', '$data[t7]', '$data[t8]', '$data[t9]', '$data[t10]', '$data[t11]', '$data[t12]', '$data[t13]', '$data[t14]', '$data[t15]', '$data[t16]', '$data[t17]', '$data[t18]', '$data[t19]', '$data[t20]', '$data[t21]', '$data[t22]', '$data[t23]', '$data[t24]')";
		$this->db->query($sql);
	}
}