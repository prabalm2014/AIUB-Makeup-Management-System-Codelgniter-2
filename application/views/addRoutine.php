<!DOCTYPE html>
<html>
<head>
	<title>Add new routine</title>
	<style type="text/css">
		.tf{
			margin-top: 10px;
			height:30px;
			width:300px;
			font-size: 16px;
		}
		.tfs{
			margin-top: 10px;
			height:38px;
			width:308px;
			font-size: 16px;
		}
		h1{
			padding-top:10px; 
			color: blue;
		}
		.frm{
			background-color: #F0F7FF;
			margin-left: 35%;
			margin-right: 35%;
		}
	</style>
</head>
<body>
	<div class="frm" align="center">
		<h1>ADD ROUTINE</h1>
		<form action="/php/main/addNewRoom" method="post">
			<select name="day" class="tfs" required>
				<option disabled="disabled" selected>Day</option>
				<option value="saturday">Saturday</option>
				<option value="sunday">Sunday</option>
				<option value="monday">Monday</option>
				<option value="tuesday">Tuesday</option>
				<option value="wednesday">Wednesday</option>
				<option value="thursday">Thursday</option>
				<option value="friday">Friday</option>
			</select><br/>
			<input class="tf" type="text" name="room" placeholder=" Room" required value=""></br>
			<input class="tf" type="text" name="t1"  placeholder=" 8.00-8.30" value=""></br>
			<input class="tf" type="text" name="t2" placeholder=" 8.30-9.00" value=""></br>
			<input class="tf" type="text" name="t3"  placeholder=" 9.00-9.30" value=""></br>
			<input class="tf" type="text" name="t4" placeholder=" 9.30-10.00" value=""></br>
			<input class="tf" type="text" name="t5"  placeholder=" 10.00-10.30" value=""></br>
			<input class="tf" type="text" name="t6" placeholder=" 10.30-11.00" value=""></br>
			<input class="tf" type="text" name="t7"  placeholder=" 11.00-11.30" value=""></br>
			<input class="tf" type="text" name="t8" placeholder=" 11.30-12.00" value=""></br>
			<input class="tf" type="text" name="t9"  placeholder=" 12.00-12.30" value=""></br>
			<input class="tf" type="text" name="t10" placeholder=" 12.30-1.00" value=""></br>
			<input class="tf" type="text" name="t11"  placeholder=" 1.00-1.30" value=""></br>
			<input class="tf" type="text" name="t12" placeholder=" 1.30-2.00" value=""></br>
			<input class="tf" type="text" name="t13"  placeholder=" 2.00-2.30" value=""></br>
			<input class="tf" type="text" name="t14" placeholder=" 2.30-3.00" value=""></br>
			<input class="tf" type="text" name="t15"  placeholder=" 3.00-3.30" value=""></br>
			<input class="tf" type="text" name="t16" placeholder=" 3.30-4.00" value=""></br>
			<input class="tf" type="text" name="t17"  placeholder=" 4.00-4.30" value=""></br>
			<input class="tf" type="text" name="t18" placeholder=" 4.30-5.00" value=""></br>
			<input class="tf" type="text" name="t19"  placeholder=" 5.00-5.30" value=""></br>
			<input class="tf" type="text" name="t20" placeholder=" 5.30-6.00" value=""></br>
			<input class="tf" type="text" name="t21"  placeholder=" 6.00-6.30" value=""></br>
			<input class="tf" type="text" name="t22" placeholder=" 6.30-7.00" value=""></br>
			<input class="tf" type="text" name="t23"  placeholder=" 7.00-7.30" value=""></br>
			<input class="tf" type="text" name="t24"  placeholder=" 7.30-8.00" value=""></br>

			<input class="tfs" name="submit" type="submit" value="ADD" /><br/>
			<a href="/php/main/admin">Back</a><br/><br/>
		</form>
	</div>
</body>
</html>