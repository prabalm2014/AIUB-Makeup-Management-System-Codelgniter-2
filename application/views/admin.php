<?php
	//$data = $this->session->userdata('open');
	//echo $data['id'];
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<style type="text/css">
			body{
				background-color: #F0F7FF;
			}
			.main{
				
				height:100%;
				width:100%;
				position:absolute;
				left: 0px;
			    top: 0px;
			}
			.side{
				height:40%;
				width:15%;
				position:fixed;
				margin-left: 3%;
				border-right:2px solid blue;
			}
			h1{
				font-size:42px;
				color:blue;
			}
			.list{
				margin-left: 20%;
				margin-top: 30%;
			}
			
			.dbod1{
				width:75%;
				margin-left: 19%;
			}
			table, th, td {
			    border-collapse: collapse;
			    background-color: #FCFAF2;
			}
			th, td {
				border: 2px solid black;
			    padding: 5px;
			}
			table{
				border: 4px solid black;
				width:100%;
			}
			.caln{
				margin-left: -40%;
				margin-top: 40%;
			}
		</style>
	</head>
	<body>
		<div class="main" align="center">
			<h1>Admin Panel</h1>
			<div class="side">
				<div class="list" align="left">
					<a href="/php/main/adprofile/<?php $data = $this->session->userdata('open'); echo $data['id']; ?>"><?php $data = $this->session->userdata('open'); echo $data['id']; ?></a><br/><br/>
					<a href="/php/main/sign">Add New Faculty</a><br/><br/>
					<a href="/php/main/addNewRoom">Add New Room/Time</a><br/><br/>
					<a href="/php/main/logout">Log Out</a><br/><br/>
					<div class="caln">
						<?php $data = $this->session->userdata('date'); echo $data['cal']; ?>
					</div>
				</div>
				
			</div>
			<div class="dbod1">
				<h2 align="left">Requested Room</h2>

				<table>
					<tr>
						<th>ID</th>
						<th>Subject</th>
						<th>Day</th>
						<th>Room</th>
						<th>Class Time</th>
						<th>Hour</th>
						<th>Faculty</th>
						<th>Date</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					{history_list}
					<tr align="center">
						<td>{h_id}</td>
						<td>{subject}</td>
						<td>{day}</td>
						<td>{room}</td>
						<td>{class_time}</td>
						<td>{hour}</td>
						<td>{User_Id}</td>
						<td>{date}</td>
						<td style="color:blue;">{status}</td>
						<td><a href="/php/main/accept/{h_id}">Accept</a>
						| <a href="/php/main/reject/{h_id}">Reject</a></td>
					</tr>
					{/history_list}
				</table>
				<br/><br/>
				<hr/>
			</div>

			<div class="dbod1">
				<h1 align="left">Sunday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list}
				</table>
			</div>
			<div class="dbod1">
				<br/>
				<h1 align="left">Monday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list1}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list1}
				</table>
			</div>
			<div class="dbod1">
				<br/>
				<h1 align="left">Tuesday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list2}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list2}
				</table>
			</div>
			<div class="dbod1">
				<br/>
				<h1 align="left">Wednesday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list3}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list3}
				</table>
			</div>
			<div class="dbod1">
				<br/>
				<h1 align="left">Thursday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list4}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list4}
				</table>
			</div>
				<div class="dbod1">
				<br/>
				<h1 align="left">Friday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list5}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list5}
				</table>
			</div>
			<div class="dbod1">
				<br/>
				<h1 align="left">Saturday</h1>
				<table>
				<tr>
					<th>Room</th>
					<th>1</th>
					<th>2</th>
					<th>3</th>
					<th>4</th>
					<th>5</th>
					<th>6</th>
					<th>7</th>
					<th>8</th>
					<th>9</th>
					<th>10</th>
					<th>11</th>
					<th>12</th>
					<th>13</th>
					<th>14</th>
					<th>15</th>
					<th>16</th>
					<th>17</th>
					<th>18</th>
					<th>19</th>
					<th>20</th>
					<th>21</th>
					<th>22</th>
					<th>23</th>
					<th>24</th>
				</tr>
				{routine_list6}
				<tr align="center">
					<td>{room}</td>
					<td>{T1}</td>
					<td>{T2}</td>
					<td>{T3}</td>
					<td>{T4}</td>
					<td>{T5}</td>
					<td>{T6}</td>
					<td>{T7}</td>
					<td>{T8}</td>
					<td>{T9}</td>
					<td>{T10}</td>
					<td>{T11}</td>
					<td>{T12}</td>
					<td>{T13}</td>
					<td>{T14}</td>
					<td>{T15}</td>
					<td>{T16}</td>
					<td>{T17}</td>
					<td>{T18}</td>
					<td>{T19}</td>
					<td>{T20}</td>
					<td>{T21}</td>
					<td>{T22}</td>
					<td>{T23}</td>
					<td>{T24}</td>
				</tr>
				{/routine_list6}
				</table>
			</div><br/><br/>
		</div>
	</body>
</html>