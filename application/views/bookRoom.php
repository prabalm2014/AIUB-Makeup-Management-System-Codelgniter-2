<!DOCTYPE html>
<html>
	<head>
		<title>Book Room</title>
		<style type="text/css">
			body{
				background-color: #F0F7FF;
			}
			fieldset{
				margin-top: 5%;
				border: 2px solid black;
				margin-left: 40%;
				margin-right: 40%;
			}
			h1{
				font-size:42px;
				color:blue;
			}
			.tf{
				margin-top: 10px;
				height:30px;
				width:300px;
				font-size: 16px;
			}
		</style>
	</head>
	<body>
		<h1 align="center">Faculty Panel</h1>
		<fieldset align="center">
		<legend>Select Day/Hour</legend>
		<form method="post" action="/php/main/addDayHour">
			<select name="day" class="tf" required>
				<option disabled="disabled" selected>Day</option>
				<option value="saturday">Saturday</option>
				<option value="sunday">Sunday</option>
				<option value="monday">Monday</option>
				<option value="tuesday">Tuesday</option>
				<option value="wednesday">Wednesday</option>
				<option value="thursday">Thursday</option>
				<option value="friday">Friday</option>
			</select><br/>

			<select name="hour" class="tf" required>
				<option disabled="disabled" selected>Hour</option>
				<option value="1.30">1.30</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select><br/><br/>

			<input type="submit" name="submit" value="Submit"><a href="/php/main/faculty">Back</a>
		</form>
	</fieldset>
	</body>
</html>