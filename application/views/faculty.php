<!DOCTYPE html>
<html>
	<head>
		<title>Faculty Panel</title>
		<style type="text/css">
			body{
				background-color: #F0F7FF;
			}
			.main{
				height:90%;
				width:100%;
				position:absolute;
				left: 0px;
			}
			.side{
				height:30%;
				width:20%;
				position:fixed;
				margin-left: 3%;
			}
			h1{
				font-size:42px;
				color:blue;
			}
			.list{
				margin-left: 25%;
				margin-right: 28%;
				margin-top: 8%;
				height: 200px;
				border-right:2px solid blue;
			}
			.dbod{
				width:70%;
				margin-left: 17%;
			}
			table, th, td {
			    border-collapse: collapse;
			    background-color: #FCFAF2;
			}
			th, td {
				border: 2px solid black;
			    padding: 5px;
			}
			table{
				border: 4px solid black;
				width:100%;
			}
		</style>
	</head>
	<body>
		<div class="main" align="center">
			<h1>Faculty Panel</h1>
			<div class="side">
				<div class="list" align="left">
					<a href="/php/main/profile/<?php $data = $this->session->userdata('open'); echo $data['id']; ?>"><?php $data = $this->session->userdata('open'); echo $data['id']; ?></a><br/><br/>
					<a href="/php/main/roomBook">Book A Room</a><br/><br/>
					<a href="/php/main/logout">Log Out</a><br/><br/>
				</div>
			</div>

			<div class="dbod">
				<br/><br/>
				<table>
					<tr>
						<th>ID</th>
						<th>Subject</th>
						<th>Day</th>
						<th>Room</th>
						<th>Class Time</th>
						<th>Hour</th>
						<th>Faculty</th>
						<th>Date</th>
						<th>Status</th>
					</tr>
					{history_list}
					<tr align="center">
						<td>{h_id}</td>
						<td>{subject}</td>
						<td>{day}</td>
						<td>{room}</td>
						<td>{class_time}</td>
						<td>{hour}</td>
						<td>{User_Id}</td>
						<td>{date}</td>
						<td>{status}</td>
					</tr>
					{/history_list}
				</table>
			</div>
		</div>
	</body>
</html>