<!DOCTYPE html>
<html>
	<head>
		<title>Profile</title>
		<style type="text/css">
			body{
				background-color: #F0F7FF;
			}
			fieldset{
				margin-top: 5%;
				border: 2px solid black;
				margin-left: 40%;
				margin-right: 40%;
			}
			h1{
				font-size:42px;
				color:blue;
			}
			th, td {
				border: 1px solid black;
			    padding: 8px;
			    color:blue;
			    font-size: 20px;
			}
			table{
				width:100%;
				background-color: #FCFAF2;
			}
		</style>
	</head>
	<body>
		<h1 align="center">Profile</h1>
		<fieldset align="center">
		<legend>Profile</legend>
		<table>
			<tr>
				<td>ID</td>
				<td>{User_Id}</td>
			</tr>
			<tr>
				<td>Name</td>
				<td>{name}</td>
			</tr>
			<tr>
				<td>Type</td>
				<td>{Type}</td>
			</tr>
			<tr>
				<td>Email</td>
				<td>{email}</td>
			</tr>
		</table><br/>
		<a href="/php/main/faculty">Back</a>
	</fieldset>
	</body>
</html>