<!DOCTYPE html>
<html>
	<head>
		<title>Book Room</title>
		<style type="text/css">
			body{
				background-color: #F0F7FF;
			}
			table, th, td {
			    border-collapse: collapse;
			}
			th, td {
				border: 1px solid black;
			    padding: 1px;
			}
			table{
				border: 2px solid black;
				width:100%;
			}
			h1{
				font-size:42px;
				color:blue;
			}
			.tf{
				margin-top: 10px;
				height:22px;
				width:250px;
				font-size: 16px;
			}
			.tfs{
				margin-top: 10px;
				height:28px;
				width:250px;
				font-size: 16px;
			}
			.calender{
				width:25%;
				background-color:#FFFFD9;
			}
			fieldset{
				width:85%;
				margin-left: 7%;
			}
		</style>
	</head>
	<body>
		<h1 align="center">Faculty Panel</h1>

		<table class="calender" align="center">
		<tr>
			<td><?php $data = $this->session->userdata('date'); echo $data['cal']; ?></td>
		</tr>
		</table><br/>

		<fieldset>
		<legend>Select Room/Time</legend>

		<form method="post" action="/php/main/request">
			<input class="tf" type="text" name="sub" placeholder=" Subject" required>
			<select name="room" class="tfs" required>
				<option disabled="disabled" selected>Room...</option>
				<option value="421">421</option>
				<option value="422">422</option>
				<option value="423">423</option>
				<option value="424">424</option>
				<option value="431">431</option>
				<option value="432">432</option>
				<option value="CL2">CL2</option>
				<option value="CL3">CL3</option>
				<option value="CL7">CL7</option>
				<option value="CL8">CL8</option>
			</select>
			<select name="time" class="tfs" required>
				<option disabled="disabled" selected>Time...</option>
				<?php
					foreach ($time_result as $result) {
				?>
				<option value="<?php echo $result['class_time']; ?>"><?php echo $result['class_time']; ?></option>
				<?php
				}
				?>
			</select>
			<input class="tf" type="text" name="date" placeholder=" Day/Month/Year" required>
			<input type="submit" name="submit" value="Submit"><br/><br/><a href="http://localhost/php/main/roomBook">Back</a>
		</form>
		<br/>
	</fieldset>

		<h1><?php $data = $this->session->userdata('dayHour'); echo $data['day']; ?></h1>
		<table>
		<tr>
			<th>Room</th>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
			<th>9</th>
			<th>10</th>
			<th>11</th>
			<th>12</th>
			<th>13</th>
			<th>14</th>
			<th>15</th>
			<th>16</th>
			<th>17</th>
			<th>18</th>
			<th>19</th>
			<th>20</th>
			<th>21</th>
			<th>22</th>
			<th>23</th>
			<th>24</th>
		</tr>
		<?php
			foreach ($routine_list as $routin) {
		?>
		<tr align="center">
			<td><?php echo $routin['room']; ?></td>
			<td><?php echo $routin['T1']; ?></td>
			<td><?php echo $routin['T2']; ?></td>
			<td><?php echo $routin['T3']; ?></td>
			<td><?php echo $routin['T4']; ?></td>
			<td><?php echo $routin['T5']; ?></td>
			<td><?php echo $routin['T6']; ?></td>
			<td><?php echo $routin['T7']; ?></td>
			<td><?php echo $routin['T8']; ?></td>
			<td><?php echo $routin['T9']; ?></td>
			<td><?php echo $routin['T10']; ?></td>
			<td><?php echo $routin['T11']; ?></td>
			<td><?php echo $routin['T12']; ?></td>
			<td><?php echo $routin['T13']; ?></td>
			<td><?php echo $routin['T14']; ?></td>
			<td><?php echo $routin['T15']; ?></td>
			<td><?php echo $routin['T16']; ?></td>
			<td><?php echo $routin['T17']; ?></td>
			<td><?php echo $routin['T18']; ?></td>
			<td><?php echo $routin['T19']; ?></td>
			<td><?php echo $routin['T20']; ?></td>
			<td><?php echo $routin['T21']; ?></td>
			<td><?php echo $routin['T22']; ?></td>
			<td><?php echo $routin['T23']; ?></td>
			<td><?php echo $routin['T24']; ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<br/><br/>
	</body>
</html>