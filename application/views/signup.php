<!DOCTYPE html>
<html>
	<head>
		<title>Sign Up</title>
		<style>
			#opa{
				margin-top: 6%;
				background-color: lavenderblush;
				margin-left: 30%;
				margin-right: 30%;
			}
			.tf{
				margin-top: 10px;
				height:30px;
				width:300px;
				font-size: 16px;
			}
			.bo{
				color: red;
			}
			h1{
				margin-bottom: -5px;
				padding-top:10px; 
			}
			.tfs{
				margin-top: 10px;
				height:38px;
				width:308px;
				font-size: 16px;
			}
		</style>
	</head>
	<body lang="en-US">
		<div id="opa" align="center">
			<h1>Sign Up</h1>
			<form action="/php/main/sign" method="post">
				<input class="tf" type="text" name="name" placeholder=" Full Name" value="<?php echo set_value('name'); ?>"></br>
				<input class="tf" type="text" name="email"  placeholder=" Email" value="<?php echo set_value('email'); ?>"></br>
				<select name="type" class="tfs">
					<option disabled="disabled" selected>Choose Member Type ....</option>
					<option value="ADMIN" <?php echo set_select('type', 'ADMIN'); ?>>ADMIN</option>
					<option value="FACULTY" <?php echo set_select('type', 'FACULTY'); ?>>FACULTY</option>
				</select></br>
				<input class="tf" type="text" name="id" placeholder=" ID" value="<?php echo set_value('id'); ?>"></br>
				<input class="tf" type="password" name="password"  placeholder=" Password"></br>
				<input class="tfs" name="signupBtn" type="submit" value="Sign Up" /><br/>
				<a href="/php/main/admin">Back</a><br/><br/>
				
				<div style="background-color: red;">
					<?php 
					echo 
					"<div style=padding:1px;>
					".validation_errors()."
					</div>"; 
					?>
				</div>
			
			</form>
		</div>
	</body>
</html>
