-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2016 at 01:08 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `makeup`
--

-- --------------------------------------------------------

--
-- Table structure for table `friday`
--

CREATE TABLE `friday` (
  `fri_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friday`
--

INSERT INTO `friday` (`fri_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '422', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '423', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '424', NULL, NULL, NULL, 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '431', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'R', 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '432', NULL, NULL, NULL, NULL, NULL, NULL, 'R', 'R', 'R', 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'CL2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'CL3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'CL7', NULL, NULL, NULL, NULL, NULL, NULL, 'R', 'R', 'R', 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'CL8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `makeuphistory`
--

CREATE TABLE `makeuphistory` (
  `h_id` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `day` varchar(200) NOT NULL,
  `room` varchar(200) NOT NULL,
  `class_time` varchar(200) NOT NULL,
  `hour` varchar(200) NOT NULL,
  `User_Id` varchar(200) NOT NULL,
  `date` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `makeuphistory`
--

INSERT INTO `makeuphistory` (`h_id`, `subject`, `day`, `room`, `class_time`, `hour`, `User_Id`, `date`, `status`) VALUES
(1, 'ATP 3', 'friday', '424', '9.30 - 11.00', '1.30', '13-23802-1', '10/05/16', 'Accepted'),
(2, 'WEB TEC', 'friday', '431', '12.00 - 2.00', '2', '13-23802-1', '10/05/17', 'Rejected'),
(3, 'ASP.NET', 'friday', 'CL7', '11.00 - 2.00', '3', '13-23802-1', '15/3/16', 'Accepted'),
(4, 'OOAD', 'tuesday', '421', '12.00 - 2.00', '2', '13-23802-1', '05/04/16', 'Accepted'),
(5, 'SQAT', 'saturday', '423', '9.30 - 11.00', '1.30', '13-23802-1', '16/03/16', 'Accepted'),
(6, 'CS MATH', 'friday', '432', '11.00 - 2.00', '3', '13-23802-1', '17/03/16', 'Accepted'),
(7, 'C#', 'thursday', 'CL7', '2.00 - 5.00', '3', '13-23802-1', '17/03/16', 'Accepted'),
(8, 'ALGO', 'saturday', '431', '11.00 - 12.30', '1.30', '11-11111-1', '16/03/16', 'Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `monday`
--

CREATE TABLE `monday` (
  `mon_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monday`
--

INSERT INTO `monday` (`mon_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC'),
(2, '422', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(3, '423', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(4, '424', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, '', 'RC', 'RC', 'RC', NULL, NULL, NULL),
(5, '431', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(6, '432', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(7, 'CL2', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'CL3', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(9, 'CL7', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'CL8', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC');

-- --------------------------------------------------------

--
-- Table structure for table `saturday`
--

CREATE TABLE `saturday` (
  `sat_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saturday`
--

INSERT INTO `saturday` (`sat_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '422', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '423', NULL, NULL, NULL, 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '424', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '431', NULL, NULL, NULL, NULL, NULL, NULL, 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'CL2', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'CL3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'CL7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'CL8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sunday`
--

CREATE TABLE `sunday` (
  `sun_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sunday`
--

INSERT INTO `sunday` (`sun_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '424', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(3, '423', 'RC', 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '424', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(5, '431', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(6, '432', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(7, 'CL2', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(8, 'CL3', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'CL7', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(10, 'CL8', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `thursday`
--

CREATE TABLE `thursday` (
  `th_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `thursday`
--

INSERT INTO `thursday` (`th_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '422', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '423', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '424', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '431', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '432', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'CL2', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'CL3', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'CL7', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'R', 'R', 'R', 'R', 'R', 'R', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'CL8', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `t_id` int(11) NOT NULL,
  `class_time` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`t_id`, `class_time`) VALUES
(1, '8.00 - 9.30'),
(2, '9.30 - 11.00'),
(3, '11.00 - 12.30'),
(4, '12.30 - 2.00'),
(5, '2.00 - 3.30'),
(6, '3.30 - 5.00'),
(7, '5.00 - 6.30'),
(8, '6.30 - 8.00'),
(9, '8.00 - 10.00'),
(10, '10.00 - 12.00'),
(11, '12.00 - 2.00'),
(12, '2.00 - 4.00'),
(13, '4.00 - 6.00'),
(14, '6.00 - 8.00'),
(15, '8.00 - 11.00'),
(16, '11.00 - 2.00'),
(17, '2.00 - 5.00'),
(18, '5.00 - 8.00');

-- --------------------------------------------------------

--
-- Table structure for table `tuesday`
--

CREATE TABLE `tuesday` (
  `tu_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuesday`
--

INSERT INTO `tuesday` (`tu_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', 'R', 'R', 'R', 'R', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '424', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(3, '423', 'RC', 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '424', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(5, '431', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(6, '432', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(7, 'CL2', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(8, 'CL3', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'CL7', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(10, 'CL8', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `User_Id` varchar(200) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `Type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`User_Id`, `Password`, `Type`, `name`, `email`) VALUES
('11-11111-1', '123456', 'FACULTY', 'tanvir', 'aiub@aiub.edu'),
('1234-123-1', '123456', 'Admin', 'Mallick', 'admin@gmail.com'),
('13-23802-1', '123456', 'Faculty', 'Prabal', 'prabal@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `wednesday`
--

CREATE TABLE `wednesday` (
  `wed_id` int(11) NOT NULL,
  `room` varchar(200) DEFAULT NULL,
  `T1` varchar(200) DEFAULT NULL,
  `T2` varchar(200) DEFAULT NULL,
  `T3` varchar(200) DEFAULT NULL,
  `T4` varchar(200) DEFAULT NULL,
  `T5` varchar(200) DEFAULT NULL,
  `T6` varchar(200) DEFAULT NULL,
  `T7` varchar(200) DEFAULT NULL,
  `T8` varchar(200) DEFAULT NULL,
  `T9` varchar(200) DEFAULT NULL,
  `T10` varchar(200) DEFAULT NULL,
  `T11` varchar(200) DEFAULT NULL,
  `T12` varchar(200) DEFAULT NULL,
  `T13` varchar(200) DEFAULT NULL,
  `T14` varchar(200) DEFAULT NULL,
  `T15` varchar(200) DEFAULT NULL,
  `T16` varchar(200) DEFAULT NULL,
  `T17` varchar(200) DEFAULT NULL,
  `T18` varchar(200) DEFAULT NULL,
  `T19` varchar(200) DEFAULT NULL,
  `T20` varchar(200) DEFAULT NULL,
  `T21` varchar(200) DEFAULT NULL,
  `T22` varchar(200) DEFAULT NULL,
  `T23` varchar(200) DEFAULT NULL,
  `T24` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wednesday`
--

INSERT INTO `wednesday` (`wed_id`, `room`, `T1`, `T2`, `T3`, `T4`, `T5`, `T6`, `T7`, `T8`, `T9`, `T10`, `T11`, `T12`, `T13`, `T14`, `T15`, `T16`, `T17`, `T18`, `T19`, `T20`, `T21`, `T22`, `T23`, `T24`) VALUES
(1, '421', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC'),
(2, '422', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL),
(3, '423', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(4, '424', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, '', 'RC', 'RC', 'RC', NULL, NULL, NULL),
(5, '431', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(6, '432', 'RC', 'RC', 'RC', NULL, 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC'),
(7, 'CL2', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'CL3', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC'),
(9, 'CL7', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'CL8', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', NULL, NULL, NULL, NULL, NULL, NULL, 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC', 'RC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friday`
--
ALTER TABLE `friday`
  ADD PRIMARY KEY (`fri_id`);

--
-- Indexes for table `makeuphistory`
--
ALTER TABLE `makeuphistory`
  ADD PRIMARY KEY (`h_id`),
  ADD KEY `User_Id` (`User_Id`);

--
-- Indexes for table `monday`
--
ALTER TABLE `monday`
  ADD PRIMARY KEY (`mon_id`);

--
-- Indexes for table `saturday`
--
ALTER TABLE `saturday`
  ADD PRIMARY KEY (`sat_id`);

--
-- Indexes for table `sunday`
--
ALTER TABLE `sunday`
  ADD PRIMARY KEY (`sun_id`);

--
-- Indexes for table `thursday`
--
ALTER TABLE `thursday`
  ADD PRIMARY KEY (`th_id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`t_id`);

--
-- Indexes for table `tuesday`
--
ALTER TABLE `tuesday`
  ADD PRIMARY KEY (`tu_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`User_Id`),
  ADD UNIQUE KEY `User_Id_UNIQUE` (`User_Id`);

--
-- Indexes for table `wednesday`
--
ALTER TABLE `wednesday`
  ADD PRIMARY KEY (`wed_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friday`
--
ALTER TABLE `friday`
  MODIFY `fri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `makeuphistory`
--
ALTER TABLE `makeuphistory`
  MODIFY `h_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `monday`
--
ALTER TABLE `monday`
  MODIFY `mon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `saturday`
--
ALTER TABLE `saturday`
  MODIFY `sat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sunday`
--
ALTER TABLE `sunday`
  MODIFY `sun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `thursday`
--
ALTER TABLE `thursday`
  MODIFY `th_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tuesday`
--
ALTER TABLE `tuesday`
  MODIFY `tu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `wednesday`
--
ALTER TABLE `wednesday`
  MODIFY `wed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `makeuphistory`
--
ALTER TABLE `makeuphistory`
  ADD CONSTRAINT `makeuphistory_ibfk_1` FOREIGN KEY (`User_Id`) REFERENCES `user` (`User_Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
